using System;
using System.Collections.Generic;
using Core.Utils.Proxifier;
using Proxifier;
using UniRx;
using Zenject;

namespace Libs.Analytics
{
    public abstract class AbstractAnalytic : IDisposable 
    {
        private readonly IList<IAnalyticable> _analyticables;
        private readonly SignalBus _signalBus;

        private readonly DiContainer _diContainer;
        
        private readonly CompositeDisposable _disposables = new CompositeDisposable();

        private readonly IList<IAnalyticHandler> _analyticHandlers = new List<IAnalyticHandler>();
        
        private readonly IList<Type> _customSignalTypes = new List<Type>();

        protected AbstractAnalytic(IList<IAnalyticable> analyticables, SignalBus signalBus, DiContainer diContainer)
        {
            _analyticables = analyticables;
            _signalBus = signalBus;
            _diContainer = diContainer;
        }

        public void Init()
        {
            _signalBus.GetStream<SignalProxyBefore>().Subscribe(s => OnSignalProxyBefore(s.MethodMessage)).AddTo(_disposables);
            _signalBus.GetStream<SignalProxyAfter>().Subscribe(s => OnSignalProxyAfter(s.ReturnMessage)).AddTo(_disposables);

            var signalTypes = SignalTypesGetter.GetSignalTypes(_diContainer);
            foreach (var signalType in signalTypes)
            {
                _signalBus.GetStream(signalType).Subscribe(HandleSignal).AddTo(_disposables);
            }

            SubscribeOnSignals(_customSignalTypes);

            HandleAnalyticables();
        }
        
        private void SubscribeOnSignals(IEnumerable<Type> signals)
        {
            foreach (var anySignal in signals)
            {
                _signalBus.GetStream(anySignal).Subscribe(HandleSignal).AddTo(_disposables);
            }
        }

        public AbstractAnalytic AddCustomSignalType(Type signalType)
        {
            _customSignalTypes.Add(signalType);
            return this;
        }

        public AbstractAnalytic AddHandler(IAnalyticHandler handler)
        {
            _analyticHandlers.Add(handler);
            return this;
        }

        public void Dispose()
        {
            _disposables?.Clear();
        }

        private void HandleSignal(object signal)
        {
            foreach (var handler in _analyticHandlers)
            {
                handler.HandleSignal(signal);
            }
        }
        
        private void OnSignalProxyBefore(IMethodMessage methodMessage)
        {
            foreach (var handler in _analyticHandlers)
            {
                handler.HandleSignalProxyBefore(methodMessage);
            }
        }
        
        private void OnSignalProxyAfter(IReturnMessage returnMessage)
        {
            foreach (var handler in _analyticHandlers)
            {
                handler.HandleSignalProxyAfter(returnMessage);
            }
        }

        private void HandleAnalyticables()
        {
            foreach (var handler in _analyticHandlers)
            {
                handler.HandleAnalyticables(_analyticables);
            } 
        }
    }
}