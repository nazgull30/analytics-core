using System.Text;
using System.Text.RegularExpressions;

namespace Libs.Analytics.NamePattern
{
    public class DefaultNamePattern : INamePattern
    {
        private readonly StringBuilder _stringBuilder = new StringBuilder();
        public string GetEventNameBySignal(string signalName) => GetOutcome(signalName, "Signal");

        public string GetEventParameterByField(string fieldName) => GetOutcome(fieldName);

        private string GetOutcome(string income, string ignored = null)
        {
            var split =  Regex.Split(income, @"(?<!^)(?=[A-Z])");

            _stringBuilder.Clear();
            for (var i = 0; i < split.Length; i++)
            {
                var part = split[i];
                var ignore = !string.IsNullOrEmpty(ignored) && part.Equals(ignored);
                if(ignore) 
                    continue;
                _stringBuilder.Append(part.ToLower());
                var notEnd = i < split.Length - 1;
                if (notEnd)
                {
                    _stringBuilder.Append("_");
                }
            }

            return _stringBuilder.ToString();
        }
    }
}