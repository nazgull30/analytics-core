namespace Libs.Analytics.NamePattern
{
    public interface INamePattern
    {
        string GetEventNameBySignal(string signalName);
        string GetEventParameterByField(string fieldName);
    }
}