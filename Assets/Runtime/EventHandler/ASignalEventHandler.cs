using System.Collections.Generic;
using System.Reflection;
using Libs.Analytics.Attributes;

namespace Libs.Analytics.EventHandler
{
	public abstract partial class AEventHandler
	{
		public void HandleSignal(object signal)
		{
			var type = signal.GetType();
			var allIgnores = type.GetCustomAttributes<IgnoreAttribute>();
			foreach (var ignore in allIgnores)
			{
				if(ignore.AnalyticServiceType == null || ignore.AnalyticServiceType == GetType()) 
					return;
			}
			
			var selfIf = type.GetCustomAttribute<SendIfAttribute>();
			var fieldIsTrue = "";
			if (selfIf != null)
			{
				if (selfIf.AnalyticServiceType == null || selfIf.AnalyticServiceType == GetType())
				{
					fieldIsTrue = selfIf.FieldIsTrue;
				}
			}

			var allOverrides = type.GetCustomAttributes<OverrideAttribute>();
			string eventName = null;
			foreach (var @override in allOverrides)
			{
				if (@override.AnalyticServiceType == null || @override.AnalyticServiceType == GetType())
				{
					eventName = @override.Name;   
				}
			}
			if (string.IsNullOrEmpty(eventName))
			{
				eventName = _namePattern.GetEventNameBySignal(signal.GetType().Name);
			}
            
			var paramsBuffer = new Dictionary<string, object>();
            
			var send = AddParametersFromType(fieldIsTrue, signal, paramsBuffer);

			if (send)
			{
				SendEvent(eventName, paramsBuffer);	
			}
		}
	}
}