using System.Collections.Generic;
using System.Reflection;
using Libs.Analytics.Attributes;
using Proxifier;

namespace Libs.Analytics.EventHandler
{
	public partial class AEventHandler
	{
		public void HandleSignalProxyAfter(IReturnMessage methodMessage)
		{
			var attr = methodMessage.MethodInfo.GetCustomAttribute<AnalyticAttribute>();
			if (attr == null) return;
			
			var allIgnores = methodMessage.MethodInfo.GetCustomAttributes<IgnoreAttribute>();
			foreach (var ignore in allIgnores)
			{
				if(ignore.AnalyticServiceType == null || ignore.AnalyticServiceType == GetType()) 
					return;
			}
			
			var selfIf = methodMessage.MethodInfo.ReturnType.GetCustomAttribute<SendIfAttribute>();
			var fieldIsTrue = "";
			if (selfIf != null)
			{
				if (selfIf.AnalyticServiceType == null || selfIf.AnalyticServiceType == GetType())
				{
					fieldIsTrue = selfIf.FieldIsTrue;
				}
			}
			
			var allOverrides = methodMessage.MethodInfo.GetCustomAttributes<OverrideAttribute>();
			string eventName = null;
			foreach (var @override in allOverrides)
			{
				if (@override.AnalyticServiceType == null || @override.AnalyticServiceType == GetType())
				{
					eventName = @override.Name;   
				}
			}
			if (string.IsNullOrEmpty(eventName))
			{
				eventName = _namePattern.GetEventNameBySignal(methodMessage.MethodInfo.ReturnType.Name);
			}

			var paramsBuffer = new Dictionary<string, object>();
			var send = AddParametersFromType(fieldIsTrue, methodMessage.ReturnValue, paramsBuffer);

			if (send)
			{
				SendEvent(eventName, paramsBuffer);	
			}
		}
	}
}