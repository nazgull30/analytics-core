using System;
using System.Collections.Generic;
using System.Reflection;
using Libs.Analytics.Attributes;
using Libs.Analytics.NamePattern;
using Proxifier;

namespace Libs.Analytics.EventHandler
{
	public abstract partial class AEventHandler : IAnalyticHandler
	{
		private readonly INamePattern _namePattern = new DefaultNamePattern();
		
		public void HandleSignalProxyBefore(IMethodMessage methodMessage)
		{
		}

		public void HandleAnalyticables(IEnumerable<IAnalyticable> analyticables)
		{
		}

		private bool AddParametersFromType (string fieldIsTrue, object instance, Dictionary<string, object> paramsBuffer)
        {
            var type = instance.GetType();
            var fields = type.GetFields();
            foreach (var field in fields)
            {
	            if(field.IsStatic)
		            continue;
	            
                var value = field.GetValue(instance);
                if (value == null) 
                    continue;

                if (field.Name.Equals(fieldIsTrue) && value is bool boolVal)
                {
	                if(!boolVal)
		                return false;
                }
                
                var ignores = field.GetCustomAttributes<IgnoreAttribute>();
                var ignoreField = false;
                foreach (var ignore in ignores)
                {
                    if (ignore.AnalyticServiceType == null || ignore.AnalyticServiceType == GetType())
                    {
                        ignoreField = true;
                        break;
                    }
                }
                if(ignoreField) 
                    continue;

                var overrideFields = field.GetCustomAttributes<OverrideAttribute>();
                string parameterName = null;
                foreach (var overrideField in overrideFields)
                {
                    if(overrideField.AnalyticServiceType == null || overrideField.AnalyticServiceType == GetType())
                    {
                        parameterName = overrideField.Name;
                    }
                    
                }
                if (string.IsNullOrEmpty(parameterName))
                {
                    parameterName = _namePattern.GetEventParameterByField(field.Name);
                }
                
                if (value is Enum)
                {
                    value = value.ToString();
                }
                
                var valueType = value.GetType();
                var isCustomType = !valueType.IsPrimitive;
                var isSystem = valueType.Namespace != null && valueType.Namespace.StartsWith("System");
                
                if (isCustomType && !isSystem)
                {
	                var selfIf = type.GetCustomAttribute<SendIfAttribute>();
	                var innerFieldIsTrue = selfIf != null ? selfIf.FieldIsTrue : "";
	                
                    AddParametersFromType(innerFieldIsTrue, value, paramsBuffer);
                    continue;
                }
                
                paramsBuffer.Add(parameterName, value);
            }
            
            return true;
        }
		
		
		protected abstract void SendEvent(string evt, Dictionary<string, object> paramsBuffer);
	}
}