using System.Collections.Generic;
using Proxifier;

namespace Libs.Analytics
{
    public interface IAnalyticHandler
    {
        void HandleSignal(object signal);

        void HandleSignalProxyBefore(IMethodMessage methodMessage);
        
        void HandleSignalProxyAfter(IReturnMessage methodMessage);

        void HandleAnalyticables(IEnumerable<IAnalyticable> analyticables);
    }
}