using System;

namespace Libs.Analytics.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class UiEventAttribute : Attribute
    {
        public string Event { get; }
        public bool ApplyPreviousWindowsNames { get; }
        public bool PublishEventWithDelay { get; }

        public UiEventAttribute(bool publishEventWithDelay = false)
        {
            PublishEventWithDelay = publishEventWithDelay;
            ApplyPreviousWindowsNames = true;
        }

        public UiEventAttribute(string @event, bool publishEventWithDelay = false, bool applyPreviousWindowsNames = true)
        {
            Event = @event;
            PublishEventWithDelay = publishEventWithDelay;
            ApplyPreviousWindowsNames = applyPreviousWindowsNames;
        }
    }
    
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Struct | AttributeTargets.Field, AllowMultiple = true)]
    public class OverrideAttribute : Attribute
    {
        public readonly Type AnalyticServiceType;
        public readonly string Name;

        public OverrideAttribute(Type analyticServiceType, string name)
        {
            AnalyticServiceType = analyticServiceType;
            Name = name;
        }

        public OverrideAttribute(string name)
        {
            Name = name;
        }
    }
    
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Struct | AttributeTargets.Field, AllowMultiple = true)]
    public class IgnoreAttribute : Attribute
    {
        public Type AnalyticServiceType;

        public IgnoreAttribute(Type analyticServiceType)
        {
            AnalyticServiceType = analyticServiceType;
        }

        public IgnoreAttribute()
        {
            AnalyticServiceType = null;
        }
    }
    
    [AttributeUsage(AttributeTargets.Struct | AttributeTargets.Field, AllowMultiple = true)]
    public class SendIfAttribute : Attribute
    {
        public Type AnalyticServiceType;
        public string FieldIsTrue;

        public SendIfAttribute(Type analyticServiceType, string fieldIsTrue)
        {
            AnalyticServiceType = analyticServiceType;
            FieldIsTrue = fieldIsTrue;
        }

        public SendIfAttribute(string fieldIsTrue)
        {
            FieldIsTrue = fieldIsTrue;
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class AnalyticAttribute : Attribute
    {
        
    }
}