using System;

namespace Libs.Analytics
{
    [AttributeUsage(AttributeTargets.Field)]
    public class SettingsAttribute : Attribute
    {
        public string Parameter { get; }

        public SettingsAttribute(string parameter)
        {
            Parameter = parameter;
        }
    }
    
    [AttributeUsage(AttributeTargets.Method)]
    public class UpdateUsernameAttribute : Attribute { }
    
}