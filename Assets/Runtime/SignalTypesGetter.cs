using System;
using System.Collections.Generic;
using Zenject;

namespace Libs.Analytics
{
	internal static class SignalTypesGetter
	{
		private static readonly List<Type> SignalTypes = new List<Type>();

		public static List<Type> GetSignalTypes(DiContainer diContainer)
		{
			if (SignalTypes.Count == 0)
			{
				var signalDeclarations = diContainer.Resolve<List<SignalDeclaration>>();
				foreach (var signalDeclaration in signalDeclarations)
				{
					var type = signalDeclaration.BindingId.Type;
					if (!type.IsInterface && typeof(ISignalAnalytic).IsAssignableFrom(type))
					{
						SignalTypes.Add(type);   
					}
				}
			}
			return SignalTypes;
		} 
	}
}