using System;
using System.Collections.Generic;

namespace Libs.Analytics
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Property)]
    public abstract class AAnalyticAttribute : Attribute
    {
        public string Event { get; }
        
        public string SendEventConditionFieldName { get; }
        
        public string[] Params { get; }
        
        public string ReturnValueParameter { get; }
        
        public bool ReturnValueParameterAsClass { get; }

        protected AAnalyticAttribute(string @event, string sendEventConditionFieldName = null)
        {
            Event = @event;
            SendEventConditionFieldName = sendEventConditionFieldName;
        }

        protected AAnalyticAttribute(string @event, string[] @params, string returnValueParameter = null)
        {
            Event = @event;
            Params = @params;
            ReturnValueParameter = returnValueParameter;
        }

        protected AAnalyticAttribute(string @event, string[] @params, bool returnValueParameterAsClass)
        {
            Event = @event;
            Params = @params;
            ReturnValueParameterAsClass = returnValueParameterAsClass;
        }

        protected AAnalyticAttribute(string @event, bool returnValueParameterAsClass)
        {
            Event = @event;
            Params = new string[0];
            ReturnValueParameterAsClass = returnValueParameterAsClass;
        }

        protected AAnalyticAttribute()
        {
        }
    }
    
    
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class SendEventConditionAttribute : Attribute
    {
        public readonly object ExpectedValue;

        public SendEventConditionAttribute(object expectedValue)
        {
            ExpectedValue = expectedValue;
        }

        public SendEventConditionAttribute()
        {
        }
    }
    
    [AttributeUsage(AttributeTargets.Struct)]
    public abstract class RevenueEventAttribute : Attribute
    {
        public string SendEventConditionFieldName;

        protected RevenueEventAttribute(string sendEventConditionFieldName)
        {
            SendEventConditionFieldName = sendEventConditionFieldName;
        }

        protected RevenueEventAttribute()
        {
        }
        
        public struct ParametersInfo
        {
            public Dictionary<string, object> Segments;
            public bool CanSendEvent;

            public ParametersInfo(Dictionary<string, object> segments, bool canSendEvent)
            {
                Segments = segments;
                CanSendEvent = canSendEvent;
            }
        }
    }
}